package com.example.eventbusdemo.observer;

import com.example.eventbusdemo.annotation.ObServerType;
import com.example.eventbusdemo.annotation.Subscribe;
import com.example.eventbusdemo.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @Author: zhangQi
 * @Date: 2020-10-07 19:02
 */
@ObServerType
public class RegNotificationObserver implements RegObserver{
    @Autowired
    private NotificationService notificationService;
    @Override
    @Subscribe
    public void handleRegSuccess(long userId) {
        notificationService.sendInBoxMessage(userId,"Welcome to my channel ~~~");
    }
}
