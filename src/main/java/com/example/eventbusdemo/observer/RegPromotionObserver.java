package com.example.eventbusdemo.observer;

import com.example.eventbusdemo.annotation.ObServerType;
import com.example.eventbusdemo.annotation.Subscribe;
import com.example.eventbusdemo.service.PromotionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: zhangQi
 * @Date: 2020-10-07 18:57
 */
@ObServerType
public class RegPromotionObserver implements RegObserver{
    @Autowired
    private PromotionService promotionService;
    @Override
    @Subscribe
    public void handleRegSuccess(long userId) {
        promotionService.issueNewUserExperienceCash(userId);
    }
}
