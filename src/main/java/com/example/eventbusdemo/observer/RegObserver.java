package com.example.eventbusdemo.observer;

/**
 * @Author: zhangQi
 * @Date: 2020-10-07 18:58
 */
public interface RegObserver {
    void handleRegSuccess(long userId);
}
