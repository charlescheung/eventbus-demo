package com.example.eventbusdemo.eventbus;

import java.util.concurrent.Executor;

/**
 * @Author: zhangQi
 * @Date: 2020-10-07 20:10
 *
 * 为实现异步非阻塞的观察者模式
 * 需要在构造函数中,由调用者注入线程池
 */
public class AsyncEventBus extends EventBus{
    public AsyncEventBus(Executor executor){
        super(executor);
    }
}
