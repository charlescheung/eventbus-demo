package com.example.eventbusdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 我们学习了观察者模式的原理,实现,应用场景,重点介绍了不同应用场景下,
 * 几种不同的实现方式,包括:同步阻塞,异步阻塞,进程内,进程间的实现方式.
 * https://www.cnblogs.com/ukzq/p/13766362.html
 * 同步阻塞是最经典的实现方式,主要是为了代码解耦;异步非阻塞除了能实现代码解耦之外,
 * 还能提高代码的执行效率;进程间的观察者模式解耦更加彻底,一般是基于消息队列来实现,
 * 用来实现不同进程间的被观察者和观察者之间的交互.
 *
 * 今天聚焦异步非阻塞的观察者模式,实现一个类似Google Guava EventBus的通用框架.
 *
 * 异步非阻塞观察者模式的简易实现code
 */
@SpringBootApplication
public class EventbusDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventbusDemoApplication.class, args);
    }

}
