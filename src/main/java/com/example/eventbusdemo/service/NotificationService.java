package com.example.eventbusdemo.service;

import org.springframework.stereotype.Service;

/**
 * @Author: zhangQi
 * @Date: 2020-10-07 19:02
 */
@Service
public class NotificationService {

    public void sendInBoxMessage(long userId,String message){
        System.out.println("给用户id为:"+userId+"的用户发送了消息:"+message);
    }
}
