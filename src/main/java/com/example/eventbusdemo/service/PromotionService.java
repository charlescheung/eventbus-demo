package com.example.eventbusdemo.service;

import org.springframework.stereotype.Service;

/**
 * @Author: zhangQi
 * @Date: 2020-10-07 18:59
 */
@Service
public class PromotionService {
    public void issueNewUserExperienceCash(long userId) {
        System.out.println("给id为:"+userId+"的用户发送新会员消息.");
    }
}
