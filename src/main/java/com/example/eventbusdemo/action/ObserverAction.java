package com.example.eventbusdemo.action;

import com.google.common.base.Preconditions;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @Author: zhangQi
 * @Date: 2020-10-07 20:01
 * ObserverAction类用来表示@Subscribe注解的方法,其中 target表示观察者类, method表示方法
 * 它主要用在ObserverRegistry观察者注册表中
 */
public class ObserverAction {
    private Object target;

    private Method method;

    public ObserverAction(Object target, Method method) {
        this.target = Preconditions.checkNotNull(target);
        this.method = method;
        this.method.setAccessible(true);
    }

    public void execute(Object event) {
        try {
            //这个观察者执行某一个event方法
            method.invoke(target, event);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
